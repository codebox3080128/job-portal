<?php 

class Candidate extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('Candidate_model');
        $this->load->model('Application_model');
    }
    
    public function register(){
        $this->form_validation->set_rules("current_ctc", "Current CTC", "required");
        $this->form_validation->set_rules("current_job_title", "Job Title", "required");
        $this->form_validation->set_rules("first_name", "First Name", "required");
        $this->form_validation->set_rules("last_name", "Last Name", "required");
        $this->form_validation->set_rules("gender", "Gender", "required");  
        $this->form_validation->set_rules("dob","Date Of Birth", "required");
        $this->form_validation->set_rules("contact", "Contact Number", "required");
        $this->form_validation->set_rules("experience", "Experience", "required");
        $this->form_validation->set_rules("current_location", "Current Location", "required");
        $this->form_validation->set_rules("email_id", "Email ID", "required|valid_email");
        $this->form_validation->set_rules("pass1", "Password", "required");
        $this->form_validation->set_rules("pass2", "Re-Password", "required|matches[pass1]");
        $this->form_validation->set_rules("terms", "Terms and condition", "required");

        if($this->form_validation->run()){
            $candidate = array(
                'first_name'=> $this->input->post('first_name'),
                'last_name'=> $this->input->post('last_name'),
                'current_job_title'=> $this->input->post('current_job_title'),
                'experience'=> $this->input->post('experience'),
                'current_ctc'=> $this->input->post('current_ctc'),
                'gender'=> $this->input->post('gender'),
                'dob'=> $this->input->post('dob'),
                'contact'=> $this->input->post('contact'),
                'current_location' =>$this->input->post('current_location'),
                'email_id' =>$this->input->post('email_id'),
                'password' =>$this->input->post('pass1'),
            );
            $this->Candidate_model->add($candidate);
            $this->session->set_flashdata('msg', 'Candidate Registered Successfully');
            redirect('candidate/login');
            exit;
        }
        else{ 
            $this->load->view('candidate/register');
        } 
    }

    public function edit(){
        if(!$this->session->userdata('candidate_id')){
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('candidate/login');
            exit;
        }
        $this->form_validation->set_rules("edit_current_ctc", "Current CTC", "required");
        $this->form_validation->set_rules("edit_first_name", "First Name", "required");
        $this->form_validation->set_rules("edit_last_name", "Last Name", "required");
        $this->form_validation->set_rules("edit_gender", "Gender", "required");  
        $this->form_validation->set_rules("edit_dob","Date Of Birth", "required");
        $this->form_validation->set_rules("edit_contact", "Contact Number", "required");
        $this->form_validation->set_rules("edit_current_location", "Current Location", "required");
        $this->form_validation->set_rules("edit_email_id", "Email ID", "required|valid_email");
        $this->form_validation->set_rules("edit_pass1", "Password", "required");
        $this->form_validation->set_rules("edit_pass2", "Re-Password", "required|matches[edit_pass1]");

        if($this->form_validation->run()){ 
            $editcandidate = array(
                'first_name'=> $this->input->post('edit_first_name'),
                'last_name'=> $this->input->post('edit_last_name'),
                'current_job_title'=> $this->input->post('edit_job_title'),
                'experience'=> $this->input->post('edit_experience'),
                'current_ctc'=> $this->input->post('edit_current_ctc'),
                'gender'=> $this->input->post('edit_gender'),
                'dob'=> $this->input->post('edit_dob'),
                'contact'=> $this->input->post('edit_contact'),
                'current_location' =>$this->input->post('edit_current_location'),
                'email_id' =>$this->input->post('edit_email_id'),
                'password' =>$this->input->post('edit_pass1'),
            );
            if(!empty($_FILES['resume']['name'])){
                $file_name = $_FILES['resume']['name'];
                $file_tmp = $_FILES['resume']['tmp_name'];
                move_uploaded_file($file_tmp, "uploads/".$file_name);
                $editcandidate['resume'] = $file_name;
            }
            $id= $this->session->userdata('candidate_id');
            $this->Candidate_model->update($editcandidate,$id);
            $this->session->set_flashdata('msg', 'Profile Updated Successfully');
            redirect('candidate/profile');
            exit;
        }
        else{
            $id = $this->session->userdata('candidate_id');
            $data['candidate']= $this->Candidate_model->get_candidate_by_id($id);
            $this->load->view('candidate/edit',$data);
        }
    }
        
    public function login(){
        $this->form_validation->set_rules('email_id', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run()){
            $credentials = array(
                'email_id' => $this->input->post('email_id'),
                'password' => $this->input->post('password'),
            );
            $candidate = $this->Candidate_model->get_candidate_by_email_and_password($credentials);
            if($candidate!=false){
                $this->session->set_userdata('first_name',$candidate['first_name']);
                $this->session->set_userdata('last_name',$candidate['last_name']);
                $this->session->set_userdata('candidate_id',$candidate['id']);
                redirect('candidate/dashboard');
                exit;
            }
            else{
                $this->session->set_flashdata('msg', 'Invalid Credentials');
                redirect('candidate/login');
                exit;
            }
        }
        else{    
            $this->load->view('candidate/login');
        }
    }

    public function dashboard(){
        if(!$this->session->userdata('candidate_id')){
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('candidate/login');
            exit;
        }
        $this->load->view('candidate/dashboard');
    }

    public function apply($id){
        $candidate_id = $this->session->userdata('candidate_id');
        $job =array(
            'job_id'=> $id,
            'candidate_id'=>$candidate_id,
            'applied_on'=> time()
        );
        $this->Application_model->add($job);
        $this->session->set_flashdata('msg', 'Job applied Successfully');
        redirect('candidate/applied_jobs');
        exit;
    }

    public function applied_jobs(){
        if(!$this->session->userdata('candidate_id')){ 
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('candidate/login');
            exit;
        }
        $candidate_id = $this->session->userdata('candidate_id');
        $data['jobs']= $this->Application_model->get_applied_jobs($candidate_id);
        $this->load->view('candidate/applied_jobs',$data);
    }
        
    public function profile(){
        if(!$this->session->userdata('candidate_id')){ 
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('candidate/login');
            exit;
        }
        $id = $this->session->userdata('candidate_id');
        $data['candidate']= $this->Candidate_model->get_candidate_by_id($id);
        $this->load->view('candidate/profile',$data);
    }

    public function download($id){
        $candidate =$this->Candidate_model->get_candidate_by_id($id);
        if(!empty($candidate['resume'])){
            $file = 'uploads/'.$candidate['resume'];
            force_download($file,NULL);
        }
        else{
            $this->session->set_flashdata('msg', "The Candidate hasn't submitted the resume");
            $id= $this->session->userdata('job_id');
            redirect('job/view_applicants/'.$id);
            exit;
        }
    }
 
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url(''));
        exit;
    }   


        
        
        






        









        







        
        

        
        
        
    }
?>