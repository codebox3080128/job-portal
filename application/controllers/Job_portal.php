<?php 
class Job_portal extends CI_Controller{

    function __construct() {
        parent::__construct();
        $this->load->model('Feedback_model');
    }

    public function index(){
        $this->load->view('job_portal/index');
    }
        
    public function register(){
            $this->load->view('job_portal/register');
    }

    public function login(){
            $this->load->view('job_portal/login');
    }
        
    public function contact_us(){
        $this->form_validation->set_rules("suggestions","Feedback", "required");
        if ($this->form_validation->run()){ 
            $suggestions= $this->input->post('suggestions');
            $this->Feedback_model->add($suggestions);
            $this->session->set_flashdata('msg', 'Feedback Submitted Successfully');
            redirect('job_portal/contact_us');
            exit;
        }
        else{
            $this->load->view('job_portal/contact');
        }

            
    }
}
?>