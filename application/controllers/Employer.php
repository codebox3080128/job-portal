<?php 
class Employer extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('Employer_model');
        $this->load->model('Job_model');
    }

    public function register(){
        $this->form_validation->set_rules("company_name","Company Name", "required");
        $this->form_validation->set_rules("company_address","Company Address", "required");
        $this->form_validation->set_rules("first_name", "First Name", "required");
        $this->form_validation->set_rules("last_name", "Last Name", "required");
        $this->form_validation->set_rules("gender", "Gender", "required");  
        $this->form_validation->set_rules("email_id", "Email ID", "required|valid_email");
        $this->form_validation->set_rules("contact", "Contact Number", "required");
        $this->form_validation->set_rules("current_location", "Current Location", "required");
        $this->form_validation->set_rules("pass1", "Password", "required");
        $this->form_validation->set_rules("pass2", "Password", "required|matches[pass1]");
        
        if ($this->form_validation->run()){ 
            $employer = array(
                'company_name'=> $this->input->post('company_name'),
                'company_address'=> $this->input->post('company_address'),
                'first_name'=> $this->input->post('first_name'),
                'last_name'=> $this->input->post('last_name'),
                'gender'=> $this->input->post('gender'),
                'contact'=> $this->input->post('contact'),
                'current_location' =>$this->input->post('current_location'),
                'email_id' =>$this->input->post('email_id'),
                'password' =>$this->input->post('pass1'),
            );
            $this->Employer_model->add($employer);
            $this->session->set_flashdata('msg', 'Employer Profile Created Successfully');
            redirect('employer/login');
            exit;
        } 
        else{
            $this->load->view('employer/register');
        } 
    }

    public function edit(){
        if(!$this->session->userdata('emp_id')){
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('employer/login');
            exit;
        }
        $this->form_validation->set_rules("edit_company_name","Company Name", "required");
        $this->form_validation->set_rules("edit_company_address","Company Address", "required");
        $this->form_validation->set_rules("edit_first_name", "First Name", "required");
        $this->form_validation->set_rules("edit_last_name", "Last Name", "required");
        $this->form_validation->set_rules("edit_gender", "Gender", "required");  
        $this->form_validation->set_rules("edit_contact", "Contact Number", "required");
        $this->form_validation->set_rules("edit_current_location", "Current Location", "required");
        $this->form_validation->set_rules("edit_email_id", "Email ID", "required|valid_email");
        $this->form_validation->set_rules("edit_pass1", "Password", "required");
        $this->form_validation->set_rules("edit_pass2", "Password", "required|matches[edit_pass1]");

        
        if($this->form_validation->run()){ 
            $editemployer = array(
                'company_name'=> $this->input->post('edit_company_name'),
                'company_address'=> $this->input->post('edit_company_address'),
                'first_name'=> $this->input->post('edit_first_name'),
                'last_name'=> $this->input->post('edit_last_name'),
                'gender'=> $this->input->post('edit_gender'),
                'contact'=> $this->input->post('edit_contact'),
                'current_location' =>$this->input->post('edit_current_location'),
                'email_id' =>$this->input->post('edit_email_id'),
                'password' =>$this->input->post('edit_pass1'),
            );
            $id= $this->session->userdata('emp_id');
            $this->Employer_model->update($editemployer,$id);
            $this->session->set_flashdata('msg', 'Profile Updated Successfully');
            redirect('employer/profile');
            exit;
        }
        else{
            $id = $this->session->userdata('emp_id');
            $data['employer']= $this->Employer_model->get_employer_by_id($id);
            $this->load->view('employer/edit',$data);
        }
    }
        
    public function login(){
        $this->form_validation->set_rules('email_id', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if($this->form_validation->run()){
            $credentials = array(
                'email_id' => $this->input->post('email_id'),
                'password' => $this->input->post('password'),
            );
            $employer= $this->Employer_model->get_employer_by_email_and_password($credentials);
            if($employer!=false){
                $this->session->set_userdata('emp_id',$employer['id']);
                $this->session->set_userdata('first_name',$employer['first_name']);
                $this->session->set_userdata('last_name',$employer['last_name']);
                redirect('employer/dashboard');
                exit;
            }
            else{
                $this->session->set_flashdata('msg', 'Invalid Credentials');
                redirect('employer/login');
                exit;
            }
        }
        else{    
            $this->load->view('employer/login');
        }
    }
        
    public function dashboard(){
        if(!$this->session->userdata('emp_id')){
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('employer/login');
            exit;
        }
        $this->load->view('employer/dashboard');
    }
       
    public function my_jobs(){
        if(!$this->session->userdata('emp_id')){
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('employer/login');
            exit;
        }
        $id = $this->session->userdata('emp_id');
        $data['jobs']= $this->Job_model->get_employer_jobs($id);
        $this->load->view('employer/myjobs',$data);
        
    }

    public function profile(){
        if (!$this->session->userdata('emp_id')){ 
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('employer/login');
            exit;
        }
        $id = $this->session->userdata('emp_id');
        $data['employer']= $this->Employer_model->get_employer_by_id($id);
        $this->session->set_flashdata('msg', 'Employer Updated Successfully');
        $this->load->view('employer/profile',$data);
    }
    
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url(''));
        exit;
    } 


        
        
        






        









        







        
        

        
        
        
    }
?>