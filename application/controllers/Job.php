<?php 
class Job extends CI_Controller{
    public function __construct() {
        parent::__construct();
        $this->load->model('Job_model');
        $this->load->model('Employer_model');
        $this->load->model('Application_model');
    }  

    public function post(){
        if(!$this->session->userdata('emp_id')){ 
            $this->session->set_flashdata('msg', 'Kindly Login First..Thank You');
            redirect('employer/login');
            exit;
        }
        $this->form_validation->set_rules("job_title", "Job Title", "required");
        $this->form_validation->set_rules("job_type", "Job Type", "required");
        $this->form_validation->set_rules("job_description", "Job Description", "required");
        $this->form_validation->set_rules("job_location", "Job Location", "required");
        $this->form_validation->set_rules("budget", "Budget", "required");
        $this->form_validation->set_rules("min_experience", "Minimum Experience", "required");

        if($this->form_validation->run()){ 
            $job = array(
                'emp_id'=>$this->session->userdata("emp_id"),
                'job_title'=> $this->input->post('job_title'),
                'job_type'=> $this->input->post('job_type'),
                'job_description'=> $this->input->post('job_description'),
                'job_location' =>$this->input->post('job_location'),
                'budget'=> $this->input->post('budget'),
                'min_experience' =>$this->input->post('min_experience'),
            );
            $this->Job_model->add($job);
            $this->session->set_flashdata('msg', 'Job posted Successfully');
            redirect('employer/my_jobs');
            exit;
        } 
        else{ 
            $this->load->view('job/post');
        } 
    }

    public function search(){
        $job_type=$this->input->post('job_type');
        $job_location=$this->input->post('job_location');
        if ($job_location =="All Locations"){
            $data['jobs']= $this->Job_model->get_all_location_jobs($job_type);
            $this->load->view('job/search',$data);
        }
        else{
            $data['jobs']= $this->Job_model->search_jobs($job_type,$job_location);
            $this->load->view('job/search',$data);
        }
    }
    
    public function details($id){
        $data['job']=$this->Job_model->get_jobs($id);
        $this->load->view('job/details',$data);
    }
    
    public function view_applicants($id){
        $this->session->set_userdata('job_id',$id); 
        $data['candidates']=$this->Application_model->view_applicants($id);
        $this->load->view('job/view_applicants',$data);
    }

    public function update($id){
        $this->form_validation->set_rules("edit_job_title", "Job Title", "required");
        $this->form_validation->set_rules("edit_job_type", "Job Type", "required");
        $this->form_validation->set_rules("edit_job_description", "Job Description", "required");
        $this->form_validation->set_rules("edit_job_location", "Job Location", "required");
        $this->form_validation->set_rules("edit_budget", "Budget", "required");
        $this->form_validation->set_rules("edit_min_experience", "Minimum Experience", "required");
                
        if($this->form_validation->run()){ 
            $editjob = array(
                'job_title'=> $this->input->post('edit_job_title'),
                'job_type'=> $this->input->post('edit_job_type'),
                'job_description'=> $this->input->post('edit_job_description'),
                'job_location' =>$this->input->post('edit_job_location'),
                'budget'=> $this->input->post('edit_budget'),
                'min_experience' =>$this->input->post('edit_min_experience')
            );
            $this->Job_model->update($editjob,$id);
            $this->session->set_flashdata('msg','Job updated Successfully');
            redirect('employer/my_jobs');
            exit;
        } 
        else{ 
            $data['job']= $this->Job_model->get_selected_job($id);
            $this->load->view('job/edit',$data);
        } 

    }

    public function remove($id){
        $this->Job_model->delete($id);
        $this->session->set_flashdata('msg', 'Job deleted Successfully');
        redirect('employer/my_jobs');
        exit;
    }


        
        
        






        









        







        
        

        
        
        
    }
?>