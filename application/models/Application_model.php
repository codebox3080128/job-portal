<?php 
   Class Application_model extends CI_Model {

   public function add($job){
      $this->db->insert('job_applications',$job);
   }

   public function get_applied_jobs($candidate_id){
      $query = $this->db->SELECT('job_title,company_name,job_location,min_experience,applied_on')
      ->from('jobs')
      ->join('job_applications','jobs.id=job_applications.job_id','LEFT')
      ->join('employers','employers.id=jobs.emp_id')
      ->where(array('candidate_id'=>$candidate_id))
      ->get();
      return $query->result_array();
   }

   public function view_applicants($id){
      $query = $this->db->SELECT('candidates.id,first_name,last_name,candidates.current_job_title,current_ctc,current_location,jobs.job_title')
      ->from('candidates')
      ->join('job_applications','job_applications.candidate_id=candidates.id','LEFT')
      ->join('jobs','job_applications.job_id=jobs.id')
      ->where(array('job_applications.job_id'=>$id))
      ->get();
      return $query->result_array();
   }

   public function delete($id){
      $this->db->delete('employers', array('id' => $id));
   }
}
