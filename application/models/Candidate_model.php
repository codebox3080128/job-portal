<?php 
Class Candidate_model extends CI_Model {

   public function add($candidate){
         
      $this->db->insert('candidates',$candidate);
   }

   public function update($editcandidate,$id){
      $this->db->where('id',$id);
      $this->db->update("candidates",$editcandidate);
   }

   public function get_candidate_by_email_and_password($credentials){
      $query = $this->db->get_where('candidates',$credentials);
      return $query->row_array();
   }
   
   public function get_candidate_by_id($id){
      $query = $this->db->select('*')->get_where('candidates',array('id'=>$id));
      return $query->row_array();
   }

   public function delete($id){
      $this->db->delete('candidates', array('id' => $id));
   }
}
