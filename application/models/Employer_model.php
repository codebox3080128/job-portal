<?php 
Class Employer_model extends CI_Model {

   public function add($employer){
      $this->db->insert('employers',$employer);
   }

   public function update($editemployer,$id){
      $this->db->where('id',$id);
      $this->db->update("employers",$editemployer);
   }

   public function get_employer_by_email_and_password($credentials){
      $query = $this->db->get_where('employers',$credentials);
      return $query->row_array();
   } 

   public function get_employer_by_id($id){
      $query = $this->db->select('*')->get_where('employers',array('id'=>$id));
      return $query->row_array();
   }

   public function delete($id){
      $this->db->delete('employers', array('id' => $id));
   }
}
