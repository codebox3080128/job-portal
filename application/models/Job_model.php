<?php 
Class Job_model extends CI_Model{
   public function add($job){
      $this->db->insert('jobs',$job);
   }

   public function update($editjob,$id){
      $this->db->where('id',$id);
      $this->db->update("jobs",$editjob);
   }

   public function get_employer_jobs($id){
      $query = $this->db->select('jobs.id,job_applications.job_id,job_title,job_location,budget,count(candidate_id)')
      ->from('jobs')
      ->join('job_applications','jobs.id=job_applications.job_id','LEFT')
      ->group_by('job_title')
      ->where(array('jobs.emp_id'=>$id))
      ->having('COUNT(candidate_id)>=',0)
      ->get();
      return $query->result_array();
      
   }

   public function get_selected_job($id){
      $query = $this->db->get_where('jobs',array('id'=>$id));
      return $query->row_array();
   }

   public function search_jobs($job_type,$job_location){
      $query = $this->db->select('jobs.id,job_title,company_name,job_location')
      ->from('jobs')
      ->where(array('job_type'=>$job_type,'job_location'=>$job_location))
      ->join('employers','jobs.emp_id=employers.id','LEFT')
      ->get();

      return $query->result_array();
   }
   public function get_all_location_jobs($job_type){
      $query = $this->db->SELECT('jobs.id,job_title,company_name,job_location')
      ->from('jobs')
      ->where(array('job_type'=>$job_type))
      ->join('employers','jobs.emp_id=employers.id','LEFT')
      ->get();

      return $query->result_array();
   }
   public function get_jobs($id){
      $query = $this->db->SELECT('jobs.id,job_title,job_description,job_location,company_name,budget')
      ->from('jobs')
      ->where(array('jobs.id'=>$id))
      ->join('employers','jobs.emp_id=employers.id','LEFT')
      ->get();

      return $query->result_array();
   }

   public function delete($id){
      $this->db->delete('jobs', array('id' => $id));
   }
}
