<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit profile</title>
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous"> -->
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/job_portal.css">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<body>
    <h1>Job Portal</h1>
    <nav class="navbar" style="background-color:  #343a40;">
    <ul class="nav nav-underline">
        <li class="nav-item">
        <a class="nav-link" aria-current="page" href="<?php echo site_url('candidate/dashboard') ?>">Dashboard</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('job/search') ?>">Search Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/applied_jobs') ?>">My Applied Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/profile') ?>">My profile</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/logout');?>">Logout</a>
        </li>
    </ul>
</nav>
<div class="container">
<div class="login-box">
    <h2>Candidate Edit profile</h2></div>
<div class="col-lg-11 well">
	<div class="row">
				<form action="<?php echo site_url('candidate/edit') ?>" method="POST" enctype="multipart/form-data">
					<div class="col-sm-10 ml-5 mt-5">
                        <div class="col-sm-6 form-group">
                            <label>Resume</label>
                            <input type="file" class="form-control" name="resume">
                            <span class="error"><span class="error"><?php echo form_error('resume'); ?></span>
                        </div>
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>First Name</label>
                                <input type="text" placeholder="Enter First Name Here.." class="form-control" name="edit_first_name" value="<?php echo $candidate['first_name'] ?>">
								<span class="error"><span class="error"><?php echo form_error('edit_first_name'); ?></span>
							</div>
							<div class="col-sm-6 form-group">
								<label>Last Name</label>
                                <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="edit_last_name" value="<?php echo $candidate['last_name'] ?>">
								<span class="error"><?php echo form_error('edit_last_name'); ?></span>
							</div>
						</div>	
                        <div class="row">
							<div class="col-sm-8 form-group">
								<label>Title</label>
                                <input type="text" placeholder="Enter Designation Here.." class="form-control" name="edit_job_title" value="<?php echo $candidate['current_job_title'] ?>">
								<span class="error"><?php echo form_error('edit_current_job_title'); ?></span>
							</div>			
							<div class="col-sm-4 form-group">
								<label>Experience</label>
                                <input type="number" placeholder="Enter your Work Experience.." class="form-control" name="edit_experience" value="<?php echo $candidate['experience'] ?>"  step="any">
								<span class="error"><?php echo form_error('edit_experience'); ?></span>
							</div>			
                            <div class="form-group col-sm-8">
                                <label>Current CTC(in lakhs per annum)</label>
                                <input type="number" placeholder="Enter your CTC here.." class="form-control" name="edit_current_ctc" value="<?php echo $candidate['current_ctc'] ?>"  step="any">
                                <span class="error"><?php echo form_error('edit_current_ctc'); ?></span>
                            </div>
						</div>
                        
						<div class="form-group">
							<label>Gender:</label>
              <input type="radio" name="edit_gender" value="Male" <?php echo ($candidate['gender'] == 'Male') ? 'checked' : ''; ?>> Male
              <input type="radio" name="edit_gender" value="Female" <?php echo ($candidate['gender'] == 'Female') ? 'checked' : ''; ?>> Female
                            <span class="error"><?php echo form_error('edit_gender'); ?></span>
                        </div>
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input type="date" placeholder="Enter Phone Number Here.." class="form-control" name="edit_dob" value="<?php echo $candidate['dob']?>">
                            <span class="error"><?php echo form_error('edit_dob'); ?></span>
                        </div>		
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="number" placeholder="Enter Phone Number Here.." class="form-control" name="edit_contact" value="<?php echo $candidate['contact']?>">
                            <span class="error"><?php echo form_error('edit_contact'); ?></span>
                        </div>		
                        <div class="form-group">
                            <label>Current Location</label>
                            <input type="text" placeholder="Enter your Current Location Here.." class="form-control" name="edit_current_location" value="<?php echo $candidate['current_location'] ?>">
                            <span class="error"><?php echo form_error('edit_current_location'); ?></span>
                        </div>		
					<div class="form-group">
						<label>Email Address</label>
						<input type="text" placeholder="Enter Email Address Here.." class="form-control" name="edit_email_id" value="<?php echo $candidate['email_id'] ?>">
                        <span class="error"><?php echo form_error('edit_email_id'); ?></span>
					</div>	
					<div class="form-group">
                        <label>Password</label>
						<input type="password" placeholder="Enter Password Here.." class="form-control" name="edit_pass1" value="<?php echo $candidate['password'] ?>">
                        <span class="error"><?php echo form_error('edit_pass1'); ?></span>
					</div>
					<div class="form-group">
                        <label>Re-Enter Password</label>
						<input type="password" placeholder="Re-Enter Password Here.." class="form-control" name="edit_pass2" value="">
                        <span class="error"><?php echo form_error('edit_pass2'); ?></span>
					</div>
					<input type="submit" class="btn btn-lg btn-info" value="Submit">					
					</div>
				</form> 
				</div>
	</div>
	</div>
<br>
  <!-- Remove the container if you want to extend the Footer to full width. -->
<div class="my-7">

<footer class="bg-dark text-center text-lg-start text-white">
  <!-- Grid container -->
  <div class="container p-3">
    <!--Grid row-->
    <div class="row mt-2">
      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Our World</h5>

        <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>About us</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>Collections</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Environmental Philosophy</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Artist Collaborations</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Assistance</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Contact us</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-backspace fa-fw fa-sm me-2"></i>Size Guide</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Shipping Informations</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Privacy policy</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Write to us</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="text-white"><i class="fas fa-at fa-fw fa-sm me-2"></i>Help us in improving</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Check the available job status</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-envelope fa-fw fa-sm me-2"></i>Join the newsletter</a>
          </li>
        </ul>
      </div>

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <h5 class="text-uppercase mb-4">Sign up to our newsletter</h5>

        <div class="form-outline form-white mb-4">
          <input type="email" id="form5Example2" class="form-control" />
          <label class="form-label" for="form5Example2">Email address</label>
        </div>

        <button type="submit" class="btn btn-outline-white btn-block">Subscribe</button>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      
      <!--Grid column-->
    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
    © 2021 Copyright:
    <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
  </div>
  <!-- Copyright -->
</footer>

</div>
<!-- End of .container -->
</body>
</head>
</html>
