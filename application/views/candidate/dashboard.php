<html>
<head>
<title>Dashboard</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/job_portal.css">

<body>
<h1>Job Portal</h1>
<nav class="navbar" style="background-color:  #343a40;">
    <ul class="nav nav-underline">
        <li class="nav-item">
        <a class="nav-link" aria-current="page" href="<?php echo site_url('candidate/dashboard') ?>">Dashboard</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('job/search') ?>">Search Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/applied_jobs') ?>">My Applied Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/profile') ?>">My profile</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/logout');?>">Logout</a>
        </li>
    </ul>
</nav>
<div class="login-box">
<h2>Dashboard</h2>
</div>
<div class="box">
    <?php if($this->session->flashdata('msg')){
            echo "<strong style='color:red'>". $this->session->flashdata('msg');
            echo '</strong>';
        } ?>
    <h4><div class="container">Welcome <?php echo $this->session->userdata('first_name')." ".$this->session->userdata('last_name')?> to Job Portal Candidate Dashboard<br></div></h4>

</div>
<br>

<!-- Remove the container if you want to extend the Footer to full width. -->
<div class="my-7">

  <footer class="bg-dark text-center text-lg-start text-white">
    <!-- Grid container -->
    <div class="container p-3">
      <!--Grid row-->
      <div class="row mt-2">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Our World</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>About us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>Collections</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Environmental Philosophy</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Artist Collaborations</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Assistance</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Contact us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-backspace fa-fw fa-sm me-2"></i>Size Guide</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Shipping Informations</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Privacy policy</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Write to us</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-at fa-fw fa-sm me-2"></i>Help us in improving</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Check the available job status</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-envelope fa-fw fa-sm me-2"></i>Join the newsletter</a>
            </li>
          </ul>
        </div>

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
          <h5 class="text-uppercase mb-4">Sign up to our newsletter</h5>

          <div class="form-outline form-white mb-4">
            <input type="email" id="form5Example2" class="form-control" />
            <label class="form-label" for="form5Example2">Email address</label>
          </div>

          <button type="submit" class="btn btn-outline-white btn-block">Subscribe</button>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2021 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>

</div>
<!-- End of .container -->
</body>
</head>
</html>
