<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/job_portal.css">
<body>
    <h1><u>Job Portal</u></h1>
    <nav class="navbar" style="background-color:  #343a40;">
<ul class="nav nav-underline">
  <li class="nav-item">
    <a class="nav-link" aria-current="page" href="<?php echo base_url('') ?>">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('job_portal/login') ?>">Login</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('job_portal/register') ?>">Sign Up</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="<?php echo site_url('job_portal/contact_us') ?>">Contact Us</a>
  </li>
</ul>
</nav>


<div class="container">
    <div class="login-box">
    <h1 class="well">Candidate Sign Up</h1>
    </div>
	<div class="col-lg-11 well">
	<div class="row">
				<form action="<?php echo site_url('candidate/register') ?>" method="POST">
					<div class="col-sm-10 ml-5 mt-5">
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>First Name</label>
                                <input type="text" placeholder="Enter First Name Here.." class="form-control" name="first_name" value="<?php echo set_value('first_name') ?>">
								<span class="error"><span class="error"><?php echo form_error('first_name'); ?></span>
							</div>
							<div class="col-sm-6 form-group">
								<label>Last Name</label>
                                <input type="text" placeholder="Enter Last Name Here.." class="form-control" name="last_name" value="<?php echo set_value('last_name') ?>">
								<span class="error"><?php echo form_error('last_name'); ?></span>
							</div>
						</div>	
                        <div class="row">
							<div class="col-sm-8 form-group">
								<label>Title</label>
                                <input type="text" placeholder="Enter Designation Here.." class="form-control" name="current_job_title" value="<?php echo set_value('current_job_title') ?>">
								<span class="error"><?php echo form_error('current_job_title'); ?></span>
							</div>			
							<div class="col-sm-4 form-group">
								<label>Experience</label>
                                <input type="number" placeholder="Enter your Work Experience.." class="form-control" name="experience" value="<?php echo set_value('experience') ?>"  step="any">
								<span class="error"><?php echo form_error('experience'); ?></span>
							</div>			
                            <div class="form-group col-sm-8">
                                <label>Current CTC(in lakhs per annum)</label>
                                <input type="number" placeholder="Enter your CTC here.." class="form-control" name="current_ctc" value="<?php echo set_value('current_ctc') ?>"  step="any">
                                <span class="error"><?php echo form_error('current_ctc'); ?></span>
                            </div>
						</div>
                        
						<div class="form-group">
							<label>Gender:</label>
                            <input type="radio" name="gender" value="Male"> Male &nbsp&nbsp
                            <input type="radio" name="gender" value="Female"> Female
                            <span class="error"><?php echo form_error('gender'); ?></span>
                        </div>
                        <div class="form-group">
                            <label>Date of Birth</label>
                            <input type="date" placeholder="Enter Phone Number Here.." class="form-control" name="dob" value="<?php echo set_value('dob') ?>">
                            <span class="error"><?php echo form_error('dob'); ?></span>
                        </div>		
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="number" placeholder="Enter Phone Number Here.." class="form-control" name="contact" value="<?php echo set_value('contact') ?>">
                            <span class="error"><?php echo form_error('contact'); ?></span>
                        </div>		
                        <div class="form-group">
                            <label>Current Location</label>
                            <input type="text" placeholder="Enter your Current Location Here.." class="form-control" name="current_location" value="<?php echo set_value('current_location') ?>">
                            <span class="error"><?php echo form_error('current_location'); ?></span>
                        </div>		
					<div class="form-group">
						<label>Email Address</label>
						<input type="text" placeholder="Enter Email Address Here.." class="form-control" name="email_id" value="<?php echo set_value('email_id') ?>">
                        <span class="error"><?php echo form_error('email_id'); ?></span>
					</div>	
					<div class="form-group">
                        <label>Password</label>
						<input type="password" placeholder="Enter Password Here.." class="form-control" name="pass1" value="<?php echo set_value('pass1') ?>">
                        <span class="error"><?php echo form_error('pass1'); ?></span>
					</div>
					<div class="form-group">
                        <label>Re-Enter Password</label>
						<input type="password" placeholder="Re-Enter Password Here.." class="form-control" name="pass2" value="<?php echo set_value('pass2') ?>">
                        <span class="error"><?php echo form_error('pass2'); ?></span>
					</div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="terms" value="yes" id="flexCheckDefault">I Agree to all Terms and Condition
                        <span class="error"><?php echo form_error('terms'); ?></span>
                    </div><br>
					<input type="submit" class="btn btn-lg btn-info" value="Submit">					
					</div>
				</form> 
				</div>
	</div>
	</div>
  <br>
  <!-- Remove the container if you want to extend the Footer to full width. -->
<div class="my-7">

<footer class="bg-dark text-center text-lg-start text-white">
  <!-- Grid container -->
  <div class="container p-3">
    <!--Grid row-->
    <div class="row mt-2">
      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Our World</h5>

        <ul class="list-unstyled mb-0">
          <li>
            <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>About us</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>Collections</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Environmental Philosophy</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Artist Collaborations</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Assistance</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Contact us</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-backspace fa-fw fa-sm me-2"></i>Size Guide</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Shipping Informations</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Privacy policy</a>
          </li>
        </ul>
      </div>
      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
        <h5 class="text-uppercase">Write to us</h5>

        <ul class="list-unstyled">
          <li>
            <a href="#!" class="text-white"><i class="fas fa-at fa-fw fa-sm me-2"></i>Help us in improving</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Check the available job status</a>
          </li>
          <li>
            <a href="#!" class="text-white"><i class="fas fa-envelope fa-fw fa-sm me-2"></i>Join the newsletter</a>
          </li>
        </ul>
      </div>

      <!--Grid column-->
      <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
        <h5 class="text-uppercase mb-4">Sign up to our newsletter</h5>

        <div class="form-outline form-white mb-4">
          <input type="email" id="form5Example2" class="form-control" />
          <label class="form-label" for="form5Example2">Email address</label>
        </div>

        <button type="submit" class="btn btn-outline-white btn-block">Subscribe</button>
      </div>
      <!--Grid column-->

      <!--Grid column-->
      
      <!--Grid column-->
    </div>
    <!--Grid row-->
  </div>
  <!-- Grid container -->

  <!-- Copyright -->
  <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
    © 2021 Copyright:
    <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
  </div>
  <!-- Copyright -->
</footer>

</div>
<!-- End of .container -->
</body>
</html>
