<html>
<head>
<title>Applied Jobs</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/job_portal.css">


<body>
<h1>Job Portal</h1>
<nav class="navbar" style="background-color:  #343a40;">
    <ul class="nav nav-underline">
        <li class="nav-item">
        <a class="nav-link" aria-current="page" href="<?php echo site_url('candidate/dashboard') ?>">Dashboard</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('job/search') ?>">Search Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/applied_jobs') ?>">My Applied Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/profile') ?>">My profile</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/logout');?>">Logout</a>
        </li>
    </ul>
</nav>
<div class="login-box">
<h2>My Profile</h2>
</div>
<a href="<?php echo site_url('candidate/edit') ?>" class="btn btn-outline-info ">Edit</a>
<br>

<?php if($this->session->flashdata('msg')){
    echo '<div class="alert alert-success" role="alert">';
    echo "<p class='message'>". $this->session->flashdata('msg')."</p>";
    echo '</div>';
    } ?>


<table class="table table-hover table-bordered table-striped">
    <tr>
        <td>Resume</td>
        <td><a href="<?php echo site_url('candidate/download/'.$this->session->userdata('candidate_id')); ?>"><?php echo $candidate['resume'] ;?></a></td>
    </tr>
    <tr>
        <td>Job title</td>
        <td><?php echo $candidate['current_job_title'] ;?></td>
    </tr>
    <tr>
        <td>Experience</td>
        <td><?php echo $candidate['experience'] ;?></td>
    </tr>
    <tr>
        <td>Current CTC</td>
        <td><?php echo $candidate['current_ctc'] ;?></td>
    </tr>
    <tr>
        <td>First Name</td>
        <td><?php echo $candidate['first_name'] ;?></td>
    </tr>
    <tr>
        <td>Last Name</td>
        <td><?php echo $candidate['last_name'] ;?></td>
    </tr>
    <tr>
        <td>Gender</td>
        <td><?php echo $candidate['gender'] ;?></td>
    </tr>
    <tr>
        <td>Date Of Birth</td>
        <td><?php echo $candidate['dob'] ;?></td>
    </tr>
    <tr>
        <td>Contact Number</td>
        <td><?php echo $candidate['contact'] ;?></td>
    </tr>
    <tr>
        <td>Current Location</td>
        <td><?php echo $candidate['current_location'] ;?></td>
    </tr>
    <tr>
        <td>Email Id</td>
        <td><?php echo $candidate['email_id'] ;?></td>
    </tr>
    </table>
<br>
<!-- Remove the container if you want to extend the Footer to full width. -->
<div class="my-7">

  <footer class="bg-dark text-center text-lg-start text-white">
    <!-- Grid container -->
    <div class="container p-3">
      <!--Grid row-->
      <div class="row mt-2">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Our World</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>About us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>Collections</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Environmental Philosophy</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Artist Collaborations</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Assistance</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Contact us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-backspace fa-fw fa-sm me-2"></i>Size Guide</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Shipping Informations</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Privacy policy</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Write to us</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-at fa-fw fa-sm me-2"></i>Help us in improving</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Check the available job status</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-envelope fa-fw fa-sm me-2"></i>Join the newsletter</a>
            </li>
          </ul>
        </div>

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
          <h5 class="text-uppercase mb-4">Sign up to our newsletter</h5>

          <div class="form-outline form-white mb-4">
            <input type="email" id="form5Example2" class="form-control" />
            <label class="form-label" for="form5Example2">Email address</label>
          </div>

          <button type="submit" class="btn btn-outline-white btn-block">Subscribe</button>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2021 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>

</div>
<!-- End of .container -->
</body>
</head>
</html>
