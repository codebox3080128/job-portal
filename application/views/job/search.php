<html>
<head>
<title>Job Search</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/job_portal.css">

<body>
<h1>Job Portal</h1>
<nav class="navbar" style="background-color:  #343a40;">
    <ul class="nav nav-underline">
        <li class="nav-item">
        <a class="nav-link" aria-current="page" href="<?php echo site_url('candidate/dashboard') ?>">Dashboard</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('job/search') ?>">Search Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/applied_jobs') ?>">My Applied Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/profile') ?>">My profile</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('candidate/logout');?>">Logout</a>
        </li>
    </ul>
</nav>
<div class="login-box">
    <h2>Job Search</h2>
</div>

<div class="search-box">
    <form action="<?php echo site_url('job/search')?>" method="POST">
        
        Job Type:
        <select class="form-select" aria-label="Default select example" name="job_type">
            <option value="full time" <?php echo set_select('job_type', 'full time'); ?>>Full Time</option>
            <option value="part time" <?php echo set_select('job_type', 'part time'); ?>>Part Time</option>
            <option value="contract based" <?php echo set_select('job_type', 'contract based'); ?>>Contract Based</option>
        </select>
        &nbsp Job Location:
        <select class="form-select" aria-label="Default select example" name="job_location">
            <option value="All Locations">All Locations</option>
            <option value="Mumbai" <?php echo set_select('job_location', 'Mumbai'); ?>>Mumbai</option>
            <option value="Pune" <?php echo set_select('job_location', 'Pune'); ?>>Pune</option>
            <option value="Delhi" <?php echo set_select('job_location', 'Delhi'); ?>>Delhi</option>
            <option value="Banglore" <?php echo set_select('job_location', 'Banglore'); ?>>Banglore</option>
            <option value="Chennai" <?php echo set_select('job_location', 'Chennai'); ?>>Chennai</option>
        </select>
        <input type="submit" value="Search">
    </form>
</div> 


    <div class="result"><h3><u>Search Results</u></h3></div>

    <table border="1" class="table table-hover table-dark table-striped">
    <tr>
        <th scope="col"> Job Title</th>
        <th scope="col">Company Name</th>
        <th scope="col">Job Location</th>
    </tr>
    
    <?php
      if (!empty($jobs)) {
        foreach ($jobs as $row) {?>
          <tr>
            <td><a class="link" href="<?= site_url('job/details/'.$row['id']) ?>"><?= $row['job_title'] ?></a></td>
            <td><?php echo $row['company_name'] ?></td>
            <td><?php echo $row['job_location'] ?></td>
        </tr>
        <?php }
      }
      else{
        echo "<tr><td colspan='3'>Sorry!! No jobs available</td></tr>";
      }?>
</table><br>

<!-- Remove the container if you want to extend the Footer to full width. -->
<div class="my-7">

  <footer class="bg-dark text-center text-lg-start text-white">
    <!-- Grid container -->
    <div class="container p-3">
      <!--Grid row-->
      <div class="row mt-2">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Our World</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>About us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>Collections</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Environmental Philosophy</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Artist Collaborations</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Assistance</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Contact us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-backspace fa-fw fa-sm me-2"></i>Size Guide</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Shipping Informations</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Privacy policy</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Write to us</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-at fa-fw fa-sm me-2"></i>Help us in improving</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Check the available job status</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-envelope fa-fw fa-sm me-2"></i>Join the newsletter</a>
            </li>
          </ul>
        </div>

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
          <h5 class="text-uppercase mb-4">Sign up to our newsletter</h5>

          <div class="form-outline form-white mb-4">
            <input type="email" id="form5Example2" class="form-control" />
            <label class="form-label" for="form5Example2">Email address</label>
          </div>

          <button type="submit" class="btn btn-outline-white btn-block">Subscribe</button>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2021 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>

</div>
<!-- End of .container -->

</body>
</head>
</html>
