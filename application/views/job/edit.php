<html>
<head>
<title>Edit Job Details</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel = "stylesheet" type = "text/css" href = "<?php echo base_url(); ?>css/job_portal.css">

<body>
<h1>Job Portal</h1>
<nav class="navbar" style="background-color:  #343a40;">
    <ul class="nav nav-underline">
        <li class="nav-item">
        <a class="nav-link" aria-current="page" href="<?php echo site_url('employer/dashboard') ?>">Dashboard</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('job/post') ?>">Post New Job</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('employer/my_jobs') ?>">My Jobs</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('employer/profile') ?>">My profile</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="<?php echo site_url('employer/logout');?>">Logout</a>
        </li>
    </ul>
</nav>
<div class="login-box">
<h2>Edit Job Details</h2>
</div>
<div class="container">       
    <form action="<?php echo site_url('job/update/'.$job['id']) ?>" method="post" >
        <div class="form-group">
            <label for="jobTitle">Job Title:</label>
            <input type="text" class="form-control" name="edit_job_title" value="<?php echo $job['job_title'] ?>" id="jobTitle" placeholder="Enter job title">
            <?php echo form_error('edit_job_title'); ?>
        </div>
        <div class="form-group">
            <label for="minExperience">Minimum Experience:</label>
            <input type="number" class="form-control"  name="edit_min_experience" value="<?php echo $job['min_experience'] ?>" id="minExperience" placeholder="Enter minimum experience">
            <?php echo form_error('edit_min_experience'); ?>
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <textarea class="form-control" name="edit_job_description"  id="description" placeholder="Enter job description"><?php echo $job['job_description'] ?></textarea>
            <?php echo form_error('edit_job_description'); ?>
        </div>
        <div class="form-group">
            <label for="budget">Budget (CTC):</label>
            <input type="number" class="form-control" name="edit_budget" value="<?php echo $job['budget'] ?>" id="budget" placeholder="Enter budget">
            <?php echo form_error('edit_budget'); ?>
        </div>
        <div class="form-group">
            <label for="jobType">Job Type:</label>
            <select name ="edit_job_type">
            <option value="full time">Full Time</option>
            <option value="part time">Part Time</option>
            <option value="contract based">Contract Based</option>
            </select>
        </div>
        <div class="form-group">
            <label for="jobLocation">Job Location:</label>
            <select name='edit_job_location'>
            <option value='Mumbai'>Mumbai</option>
            <option value='Pune'>Pune</option>
            <option value='Delhi'>Delhi</option>
            <option value='Banglore'>Banglore</option>
            <option value='Chennai'>Chennai</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
        </form>

</div><br>


<!-- Remove the container if you want to extend the Footer to full width. -->
<div class="my-7">

  <footer class="bg-dark text-center text-lg-start text-white">
    <!-- Grid container -->
    <div class="container p-3">
      <!--Grid row-->
      <div class="row mt-2">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Our World</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>About us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-book fa-fw fa-sm me-2"></i>Collections</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Environmental Philosophy</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-user-edit fa-fw fa-sm me-2"></i>Artist Collaborations</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Assistance</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Contact us</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-backspace fa-fw fa-sm me-2"></i>Size Guide</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Shipping Informations</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="far fa-file-alt fa-fw fa-sm me-2"></i>Privacy policy</a>
            </li>
          </ul>
        </div>
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h5 class="text-uppercase">Write to us</h5>

          <ul class="list-unstyled">
            <li>
              <a href="#!" class="text-white"><i class="fas fa-at fa-fw fa-sm me-2"></i>Help us in improving</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-shipping-fast fa-fw fa-sm me-2"></i>Check the available job status</a>
            </li>
            <li>
              <a href="#!" class="text-white"><i class="fas fa-envelope fa-fw fa-sm me-2"></i>Join the newsletter</a>
            </li>
          </ul>
        </div>

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-lg-0">
          <h5 class="text-uppercase mb-4">Sign up to our newsletter</h5>

          <div class="form-outline form-white mb-4">
            <input type="email" id="form5Example2" class="form-control" />
            <label class="form-label" for="form5Example2">Email address</label>
          </div>

          <button type="submit" class="btn btn-outline-white btn-block">Subscribe</button>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </div>
    <!-- Grid container -->

    <!-- Copyright -->
    <div class="text-center p-3" style="background-color: rgba(0, 0, 0, 0.2)">
      © 2021 Copyright:
      <a class="text-white" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
  </footer>

</div>
<!-- End of .container -->
</body>
</head>
</html>
